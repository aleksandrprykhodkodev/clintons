(function ($) {
  $(document.body).on('load', function () {
    top.location.href = '#block-slideshow';
  });
  $(document).ready(function () {
    $('#block-views-block-team-type-block-1').css('display', 'block');
    $('#people').css('display', 'block');
  });
})(jQuery);

// anchor links
(function ($) {
  $(document).ready(function () {
    $("nav .navbar-nav .nav-item").on("click", "a", function (event) {
      event.preventDefault();
      $("nav .navbar-nav .nav-item a").removeClass('active');
      $(this).addClass('active');
      var id = $(this).attr('href'),
        top = $(id).offset().top;
      $('body,html').animate({
        scrollTop: top
      }, 1500);
    });
  });
})(jQuery);

(function ($) {
  $(document).ready(function () {
    $('.carousel-inner .carousel-item:first').addClass('active');
  });
})(jQuery);

// carousel
(function ($) {
  jQuery('#carouselPeople .carousel-inner > article').wrap('<div class="carousel-item col-md-3" />');
  $('#carouselPeople').on('slide.bs.carousel', function (e) {
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 5;
    var totalItems = $('.carousel-item').length;

    if (idx >= totalItems-(itemsPerSlide-1)) {
      var it = itemsPerSlide - (totalItems - idx);
      for (var i=0; i<it; i++) {
        // append slides to end
        if (e.direction=="left") {
          $('.carousel-item').eq(i).appendTo('.carousel-inner');
        }
        else {
          $('.carousel-item').eq(0).appendTo('.carousel-inner');
        }
      }
    }
  });

  currentSlide = Math.floor((Math.random() * $('.carousel-item').length));
  rand = currentSlide;
  $('#carouselPeople').carousel(currentSlide);
  $('#carouselPeople').fadeIn(1000);
  setInterval(function(){
    while(rand == currentSlide){
      rand = Math.floor((Math.random() * $('.carousel-item').length));
    }
    currentSlide = rand;
    $('#carouselPeople').carousel(rand);
  }, 1000000);

  $('#carouselPeople').carousel({
    interval: 2000
  });

  var currentSlide;
  var rand;
})(jQuery);

(function ($) {
  $(document).ready(function () {
    /* show lightbox when clicking a thumbnail */
    $('a.thumb').click(function (event) {
      event.preventDefault();
      var content = $('.modal-body');
      content.empty();
      var title = $(this).attr("title");
      $('.modal-title').html(title);
      content.html($(this).html());
      $(".modal-profile").modal({
        show: true
      });
    });
  });
})(jQuery);

(function ($) {
  $(".column-one .list-block:odd").css('display', 'none');
  $(".column-two .list-block:even").css('display', 'none');

  $(function () {
    $("#block-views-block-team-type-block-1 .ui-accordion").accordion({
      collapsible: true,
      active: false,
      show: {
        effect: "slideDown",
        duration: "slow"
      },
      hide: {
        effect: "slideUp",
        duration: "slow"
      }
    });
  });

  $(function($) {
    $('#block-views-block-team-type-block-1').append('<div class="q-content"></div>');
    $("#block-views-block-team-type-block-1 .ui-accordion>div>h3").click(function () {
      var popup = $(this).next('.ui-accordion-content');
      var clone = popup.clone();
      popup.css('display', 'none');
      $('.q-content').empty();
      clone.appendTo($('.q-content'));
      if(this.classList.contains('ui-accordion-header-active')){
        $(this).attr('aria-expanded','true');
      }
      else {
        $(this).attr('aria-expanded','false');
      }
    });
  });

  jQuery(function($){
    $(document).mouseup(function (e){
      var block = $("#drupal-modal");
      if (!block.is(e.target)
        && block.has(e.target).length === 0) {
        block.remove();
        $(".ui-widget-overlay").css('background', 'transparent');
      }
    });
  });

  $(document).ready(function(){
    var items = $(".list-text h1");
    var delay = 2000;
    var id = 0;

    var text = $(".text-animation .text");

    $('div.text h1').text($(items[id]).text());

    function cycle() {
      text.animate({
        'width': "20px",
      }, 2000 );

      text.queue(function () {
        changeText();
        $(this).dequeue();
      });
      text.animate({
        'width': "98%",
      }, 2000 );
    }

    function changeText( ) {
      ++id;

      if(typeof items[id] == typeof undefined){
        id = 0;
      }

      $('div.text h1').text($(items[id]).text());
      var colors = {0:"#f7c457",1:"#d85989",2:"#7600de"};
      setInterval(function() {
        $("div.text h1").css("color",colors[id]);
      }, 4100);
    }
    setInterval(cycle, delay);
  });

  function parallax(){
    var scrolled = $(window).scrollTop();
    $('.border-left').css('top', +(scrolled * 0.3) + 'px');
    $('.border-right').css('bottom', -(scrolled * 0.3) + 'px');
  }

  $(window).scroll(function(e){
    parallax();
  });

  jQuery('.list-item').click( function(e) {
    jQuery('.collapse').collapse('hide');
  });

  $(document).ready(function(){
    var items = $(".list-text h1");
    var delay = 2000;
    var id = 0;

    var text = $(".text-animation .text");

    $('div.text h1').text($(items[id]).text());

    function cycle() {
      text.animate({
        'width': "20px",
      }, 2000 );

      text.queue(function () {
        changeText();
        $(this).dequeue();
      });
      text.animate({
        'width': "98%",
      }, 2000 );
    }

    function changeText( ) {
      ++id;

      if(typeof items[id] == typeof undefined){
        id = 0;
      }

      $('div.text h1').text($(items[id]).text());
      var colors = {0:"#f7c457",1:"#d85989",2:"#7600de"};
      setInterval(function() {
        $("div.text h1").css("color",colors[id]);
      }, 4100);
    }
    setInterval(cycle, delay);
  });
})(jQuery);